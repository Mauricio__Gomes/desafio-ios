//
//  GHProjectResult.h
//  GitHub JavaPop
//
//  Created by Maurício Gomes on 9/11/16.
//  Copyright © 2016 Mauricio Gomes. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "GHProjectOwner.h"

@interface GHProjectResult : NSObject

@property (nonatomic, strong) NSString* name;
@property (nonatomic, strong) NSString* projectDescription;
@property (nonatomic, strong) GHProjectOwner* owner;
@property (nonatomic) NSInteger forks;
@property (nonatomic) NSInteger stars;

- (id)initWithDictionary:(NSDictionary*)dictionary;

@end
