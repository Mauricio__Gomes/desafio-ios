//
//  DetailViewController.m
//  GitHub JavaPop
//
//  Created by Maurício Gomes on 9/9/16.
//  Copyright © 2016 Mauricio Gomes. All rights reserved.
//

#import "DetailViewController.h"
#import "GHPullRequest.h"
#import <WebImage/UIImageView+WebCache.h>

@interface DetailViewController ()

@end

@implementation DetailViewController

#pragma mark - Managing the detail item

- (void)setPullRequestsURL:(id)pullURL
{
    if(_loaded != YES)
    {
        _queueSetPullRequestURL = pullURL;
    }
    
    if (_pullURL != pullURL)
    {
        _pullURL = pullURL;
        [self resetTable];
    }
}

- (void)resetTable
{
    _pullRequestsPages = [[NSMutableArray alloc] init];
    [_tableView reloadData];
    _headerMessage.text = @"Carregando";
    _rows = 0;
    _pagesFetched = 0;
    _rowsPerPage = 0;
    _closedPulls = 0;
    _openPulls = 0;
    _endedFetching = NO;
    _cellHeight = 0;
    _highestRowRequested = 0;
    
    if(_pullURL == nil)
    {
        _headerMessage.text = @"Selecionar Repositório";
        return;
    }
    
    [self fetchPullRequestPage];
}

- (void)fetchPullRequestPage
{
    if(_APIbusy || _endedFetching) return;
    
    _APIbusy = YES;
    
    NSURL *url = [NSURL URLWithString:[NSString stringWithFormat:@"%@&page=%d", _pullURL, _pagesFetched]];
    NSURLRequest *request = [NSURLRequest requestWithURL:url];
    [NSURLConnection sendAsynchronousRequest:request
                                       queue:[NSOperationQueue mainQueue]
                           completionHandler:^(NSURLResponse *response,
                                               NSData *data, NSError *connectionError)
     {
         if (data.length > 0 && connectionError == nil)
         {
             id resultsDictionary = [NSJSONSerialization JSONObjectWithData:data
                                                                    options:0
                                                                      error:NULL];
             
             if(![resultsDictionary isKindOfClass:[NSArray class]])
             {
                 _headerMessage.text = @"Limite de chamadas da API atingido";
                 _APIbusy = NO;
                 return;
             }
             
             [_pullRequestsPages addObject:[[GHPullRequestsList alloc] initWithArray:resultsDictionary]];
             ++_pagesFetched;
             [self addCells];
             _APIbusy = NO; //to be sure we only get available AFTER we messed with the interface.
         }
         else
         {
             _APIbusy = NO;
             _headerMessage.text = @"Erro";
         }
     }];
}

- (void)addCells
{
    GHPullRequestsList* lastResults = self.pullRequestsPages.lastObject;
    NSInteger pullRequestsOnPage = lastResults.requestsArray.count;
    
    NSMutableArray* indexPathArray = [[NSMutableArray alloc] init];
    int counter = 0;
    while(counter < pullRequestsOnPage)
    {
        indexPathArray[counter] = [NSIndexPath indexPathForRow:counter inSection:0];
        GHPullRequest* pullRequest = lastResults.requestsArray[counter];
        if(pullRequest.closed)
        {
            self.closedPulls += 1;
        }
        else
        {
            self.openPulls += 1;
        }
        ++self.rows;
        ++counter;
    }
    if(self.pagesFetched > 1 && counter < self.rowsPerPage)
    {
        //end of the pages!
        self.endedFetching = YES;
    }
    else
    {
        self.rowsPerPage = self.rows / self.pagesFetched;
    }
    
    if(self.endedFetching || (self.pagesFetched == 1 && self.rows < 30)) //yes, hardcoded row limit... :/ should think a way to fix this. anyway, 30 is the current GitHub API default for rows in a page
    {
        _headerMessage.text = [NSString stringWithFormat:@"Total %d - %d abertos / %d fechados", self.rows, self.openPulls, self.closedPulls];
    }
    else
    {
        _headerMessage.text = [NSString stringWithFormat:@"Carregando - %d abertos / %d fechados", self.openPulls, self.closedPulls];
    }
    [self.tableView insertRowsAtIndexPaths:indexPathArray withRowAnimation:UITableViewRowAnimationNone];
}

- (id)init
{
    self = [super init];
    _loaded = NO;
    _queueSetPullRequestURL = nil;
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    _pullURL = nil;
    [self resetTable];
    _loaded = YES;
    if(_queueSetPullRequestURL != nil)
    {
        [self setPullRequestsURL:_queueSetPullRequestURL];
        _queueSetPullRequestURL = nil;
    }
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
}

#pragma mark - Table View

- (void)scrollViewDidScroll:(UIScrollView *)tableView
{
    if(_pullURL == nil || _loaded == NO || _APIbusy == YES || _endedFetching == YES || self.rows == 0) return;
    
    if(tableView.contentOffset.y > tableView.contentSize.height - tableView.frame.size.height*2)
    {
        CGFloat cellsPer2Screens = (tableView.frame.size.height*2)/self.cellHeight;
        if(self.rows - self.highestRowRequested < cellsPer2Screens)
        {
            [self fetchPullRequestPage];
        }
    }
}

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
    return 1;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    return self.rows;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:@"Cell" forIndexPath:indexPath];
    
    NSInteger currentPage = indexPath.row/self.rowsPerPage;
    NSInteger currentRowInPage = indexPath.row%self.rowsPerPage;
    GHPullRequestsList* searchResultsPage = self.pullRequestsPages[currentPage];
    GHPullRequest* pullRequest = searchResultsPage.requestsArray[currentRowInPage];
    ((UILabel *)[cell viewWithTag:1]).text = pullRequest.title;
    ((UILabel *)[cell viewWithTag:2]).text = pullRequest.contents;
    ((UILabel *)[cell viewWithTag:3]).text = pullRequest.owner.username;
    [[cell viewWithTag:4] sd_setImageWithURL:[NSURL URLWithString:pullRequest.owner.avatarURL]
                            placeholderImage:[UIImage imageNamed:@"placeholder.png"]];
    
    self.cellHeight = cell.frame.size.height;
    if(indexPath.row > self.highestRowRequested) self.highestRowRequested = indexPath.row;
    
    return cell;
}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    NSInteger currentPage = indexPath.row/self.rowsPerPage;
    NSInteger currentRowInPage = indexPath.row%self.rowsPerPage;
    GHPullRequestsList* searchResultsPage = self.pullRequestsPages[currentPage];
    GHPullRequest* pullRequest = searchResultsPage.requestsArray[currentRowInPage];
    
    //the next four lines are necessary because iOS clear the cell when running this function, to it is needed to "unclear" it again.
    UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:@"Cell" forIndexPath:indexPath];
    ((UILabel *)[cell viewWithTag:1]).text = pullRequest.title;
    ((UILabel *)[cell viewWithTag:2]).text = pullRequest.contents;
    ((UILabel *)[cell viewWithTag:3]).text = pullRequest.owner.username;
    [[cell viewWithTag:4] sd_setImageWithURL:[NSURL URLWithString:pullRequest.owner.avatarURL]
                            placeholderImage:[UIImage imageNamed:@"placeholder.png"]];
    
    NSURL *url = [NSURL URLWithString:pullRequest.pullURL];
    [[UIApplication sharedApplication] openURL:url];
}


@end
