//
//  GHPullRequest.m
//  GitHub JavaPop
//
//  Created by Maurício Gomes on 9/12/16.
//  Copyright © 2016 Mauricio Gomes. All rights reserved.
//

#import "GHPullRequest.h"

@implementation GHPullRequest

- (id)initWithDictionary:(NSDictionary*)dictionary
{
    self = [super init];
    
    id titleObject = [dictionary objectForKey:@"title"];
    if(titleObject == [NSNull null] || titleObject == nil)
    {
        self.title = @"NULL TITLE";
    }
    else
    {
        self.title = (NSString*)titleObject;
    }
    
    id bodyObject = [dictionary objectForKey:@"body"];
    if(bodyObject == [NSNull null] || bodyObject == nil)
    {
        self.contents = @"NULL BODY";
    }
    else
    {
        self.contents = (NSString*)bodyObject;
    }
    
    id owner = [dictionary objectForKey:@"user"];
    
    id stateObject = [dictionary objectForKey:@"state"];
    if(stateObject == [NSNull null] || stateObject == nil) //consider "not closed"
    {
        self.closed = NO;
    }
    else
    {
        if([(NSString*)stateObject isEqualToString:@"closed"])
        {
            self.closed = YES;
        }
        else
        {
            self.closed = NO;
        }
    }
    self.owner = [[GHProjectOwner alloc] initWithDictionary:(NSDictionary*)owner];
    
    id urlObject = [dictionary objectForKey:@"html_url"];
    if(urlObject == [NSNull null] || urlObject == nil)
    {
        self.pullURL = @"NULL URL";
    }
    else
    {
        self.pullURL = (NSString*)urlObject;
    }
    
    return self;
}

@end
