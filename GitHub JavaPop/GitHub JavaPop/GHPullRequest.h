//
//  GHPullRequest.h
//  GitHub JavaPop
//
//  Created by Maurício Gomes on 9/12/16.
//  Copyright © 2016 Mauricio Gomes. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "GHProjectOwner.h"

@interface GHPullRequest : NSObject

@property (nonatomic, strong) NSString* title;
@property (nonatomic, strong) NSString* contents;
@property (nonatomic, strong) NSString* pullURL;
@property (nonatomic, strong) GHProjectOwner* owner;
@property (nonatomic) BOOL closed;

- (id)initWithDictionary:(NSDictionary*)dictionary;

@end
