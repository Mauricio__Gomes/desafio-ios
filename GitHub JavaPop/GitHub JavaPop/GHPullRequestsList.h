//
//  GHPullRequestsList.h
//  GitHub JavaPop
//
//  Created by Maurício Gomes on 9/11/16.
//  Copyright © 2016 Mauricio Gomes. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface GHPullRequestsList : NSObject

@property (strong, nonatomic) NSMutableArray* requestsArray;

- (id)initWithArray:(NSArray*)resultsArray;

@end
