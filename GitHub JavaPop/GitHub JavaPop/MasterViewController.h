//
//  MasterViewController.h
//  GitHub JavaPop
//
//  Created by Maurício Gomes on 9/9/16.
//  Copyright © 2016 Mauricio Gomes. All rights reserved.
//

#import <UIKit/UIKit.h>

@class DetailViewController;

@interface MasterViewController : UITableViewController

@property (strong, nonatomic) DetailViewController *detailViewController;
@property (strong, nonatomic) NSMutableArray* projectSearchResults;
@property (nonatomic) NSInteger rows;
@property (nonatomic) NSInteger rowsPerPage;
@property (nonatomic) NSInteger pagesFetched;
@property (nonatomic) NSInteger highestRowRequested;
@property (nonatomic) CGFloat cellHeight;
@property (nonatomic) BOOL APIbusy;
@property (nonatomic) BOOL endedFetching;
@property (strong, nonatomic) NSMutableDictionary* namesCache;
@property (strong, nonatomic) NSMutableDictionary* avatarsCache;

@end

