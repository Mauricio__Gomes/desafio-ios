//
//  GHProjectResult.m
//  GitHub JavaPop
//
//  Created by Maurício Gomes on 9/11/16.
//  Copyright © 2016 Mauricio Gomes. All rights reserved.
//

#import "GHProjectResult.h"

@implementation GHProjectResult

- (id)initWithDictionary:(NSDictionary*)dictionary
{
    self = [super init];
    
    id nameObject = [dictionary objectForKey:@"name"];
    if(nameObject == [NSNull null] || nameObject == nil)
    {
        self.name = @"NULL NAME";
    }
    else
    {
        self.name = (NSString*)nameObject;
    }
    id descriptionObject = [dictionary objectForKey:@"description"];
    if(descriptionObject == [NSNull null] || descriptionObject == nil) //platform_frameworks_base you are a very, very evil project, NULL description!
    {
        self.projectDescription = @"NULL DESCRIPTION";
    }
    else
    {
        self.projectDescription = (NSString*)descriptionObject; //NSObjects already have a field named "description" thus the name being longer.
    }
    id projectOwner = [dictionary objectForKey:@"owner"];
    self.owner = [[GHProjectOwner alloc] initWithDictionary:(NSDictionary*)projectOwner];
    self.forks = [[dictionary objectForKey:@"forks_count"] integerValue];
    self.stars = [[dictionary objectForKey:@"stargazers_count"] integerValue];
    return self;
}

@end
