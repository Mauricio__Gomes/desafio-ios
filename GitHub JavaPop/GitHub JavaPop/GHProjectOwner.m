//
//  GHProjectOwner.m
//  GitHub JavaPop
//
//  Created by Maurício Gomes on 9/11/16.
//  Copyright © 2016 Mauricio Gomes. All rights reserved.
//

#import "GHProjectOwner.h"

@implementation GHProjectOwner

- (id)initWithDictionary:(NSDictionary*)dictionary
{
    self = [super init];
    
    id usernameObject = [dictionary objectForKey:@"login"];
    if(usernameObject == [NSNull null] || usernameObject == nil)
    {
        self.username = @"NULL USERNAME";
    }
    else
    {
        self.username = (NSString*)usernameObject;
    }
    
    id avatarURLObject = [dictionary objectForKey:@"avatar_url"];
    if(avatarURLObject == [NSNull null] || avatarURLObject == nil)
    {
        self.avatarURL = @"NULL AVATAR";
    }
    else
    {
        self.avatarURL = (NSString*)avatarURLObject;
    }
    
    id profileURLObject = [dictionary objectForKey:@"url"];
    if(profileURLObject == [NSNull null] || profileURLObject == nil)
    {
        self.profileURL = @"NULL PROFILE URL";
    }
    else
    {
        self.profileURL = (NSString*)profileURLObject;
    }
    
    return self;
}

@end
