//
//  AppDelegate.h
//  GitHub JavaPop
//
//  Created by Maurício Gomes on 9/9/16.
//  Copyright © 2016 Mauricio Gomes. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface AppDelegate : UIResponder <UIApplicationDelegate>

@property (strong, nonatomic) UIWindow *window;


@end

