//
//  main.m
//  GitHub JavaPop
//
//  Created by Maurício Gomes on 9/9/16.
//  Copyright © 2016 Mauricio Gomes. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "AppDelegate.h"

int main(int argc, char * argv[]) {
    @autoreleasepool {
        return UIApplicationMain(argc, argv, nil, NSStringFromClass([AppDelegate class]));
    }
}
