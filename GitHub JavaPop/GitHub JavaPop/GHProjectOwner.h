//
//  GHProjectOwner.h
//  GitHub JavaPop
//
//  Created by Maurício Gomes on 9/11/16.
//  Copyright © 2016 Mauricio Gomes. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface GHProjectOwner : NSObject

@property (nonatomic, strong) NSString* username;
@property (nonatomic, strong) NSString* avatarURL;
@property (nonatomic, strong) NSString* profileURL;

- (id)initWithDictionary:(NSDictionary*)dictionary;

@end
