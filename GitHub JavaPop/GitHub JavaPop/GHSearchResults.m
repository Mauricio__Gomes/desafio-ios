//
//  GHSearchResults.m
//  GitHub JavaPop
//
//  Created by Maurício Gomes on 9/11/16.
//  Copyright © 2016 Mauricio Gomes. All rights reserved.
//

#import "GHSearchResults.h"
#import "GHProjectResult.h"

@implementation GHSearchResults

- (id)initWithDictionary:(NSDictionary*)dictionary
{
    self = [super init];
    
    self.totalResultsCount = [[dictionary objectForKey:@"total_count"] integerValue];
    self.resultsAreImcomplete = [[dictionary objectForKey:@"incomplete_results"] boolValue];
    NSArray* projectsArray = [dictionary objectForKey:@"items"];
    self.projects = [[NSMutableArray alloc] init];
    NSInteger counter = 0;
    while(counter < projectsArray.count)
    {
        [self.projects addObject:[[GHProjectResult alloc] initWithDictionary:projectsArray[counter]]];
        ++counter;
    }
    
    return self;
}

@end
