//
//  GHSearchResults.h
//  GitHub JavaPop
//
//  Created by Maurício Gomes on 9/11/16.
//  Copyright © 2016 Mauricio Gomes. All rights reserved.
//

//After some tests with Mantle, including some that not ended in Version control, I decided that Mantle was too "bloated" to do object mapping that is strictly read-only.
//Since the project doesn't require uploading data, only downloading simple data and some pictures, I concluded that doing "by hand" was better.

#import <Foundation/Foundation.h>

@interface GHSearchResults : NSObject

@property (nonatomic) NSInteger totalResultsCount;
@property (nonatomic) BOOL resultsAreImcomplete;
@property (nonatomic, strong) NSMutableArray* projects;

- (id)initWithDictionary:(NSDictionary*)dictionary;

@end
