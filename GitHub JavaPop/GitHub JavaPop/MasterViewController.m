//
//  MasterViewController.m
//  GitHub JavaPop
//
//  Created by Maurício Gomes on 9/9/16.
//  Copyright © 2016 Mauricio Gomes. All rights reserved.
//

#import "MasterViewController.h"
#import "DetailViewController.h"
#import "GHSearchResults.h"
#import "GHProjectResult.h"
#import <WebImage/UIImageView+WebCache.h>

@implementation MasterViewController

- (void)fetchGHSearchPage:(NSInteger)pageNumber
{
    if(self.APIbusy || self.endedFetching) return;
    self.APIbusy = YES;
    if(pageNumber-1 > [self.projectSearchResults count]) pageNumber = [self.projectSearchResults count]+1;
    NSURL *url = [NSURL URLWithString:[NSString stringWithFormat:@"https://api.github.com/search/repositories?q=language:Java&sort=stars&page=%d", pageNumber]];
    //NSURL *url = [NSURL URLWithString:[NSString stringWithFormat:@"https://api.github.com/search/repositories?q=reacty&sort=stars&page=%d", pageNumber]]; //this URL has about 95 results, I used it to test what happens in my code when the results end, uncomment it if you want to test it too.
    NSURLRequest *request = [NSURLRequest requestWithURL:url];
    [NSURLConnection sendAsynchronousRequest:request
                                       queue:[NSOperationQueue mainQueue]
                           completionHandler:^(NSURLResponse *response,
                                               NSData *data, NSError *connectionError)
     {
         if (data.length > 0 && connectionError == nil)
         {
             NSDictionary *resultsDictionary = [NSJSONSerialization JSONObjectWithData:data
                                                                      options:0
                                                                        error:NULL];
             GHSearchResults* searchResults = [[GHSearchResults alloc] initWithDictionary:resultsDictionary];
             [self.projectSearchResults insertObject:searchResults atIndex:pageNumber-1];
             self.pagesFetched += 1;
             [self addPageOfRows];
             self.APIbusy = NO;
             
         }
     }];
}

- (void)addPageOfRows
{
    GHSearchResults* lastResults = self.projectSearchResults.lastObject;
    NSInteger projectsFetchedOnPage = lastResults.projects.count;
    
    NSMutableArray* indexPathArray = [[NSMutableArray alloc] init];
    int counter = 0;
    while(counter < projectsFetchedOnPage)
    {
        indexPathArray[counter] = [NSIndexPath indexPathForRow:counter inSection:0];
        GHProjectResult* project = lastResults.projects[counter];
        [self fetchRealNameForLogin:project.owner.username usingURL:project.owner.profileURL];
        ++self.rows;
        ++counter;
    }
    if(self.pagesFetched > 1 && counter < self.rowsPerPage)
    {
        //end of the pages!
        self.endedFetching = YES;
    }
    else
    {
        self.rowsPerPage = self.rows / self.pagesFetched;
    }
    [self.tableView insertRowsAtIndexPaths:indexPathArray withRowAnimation:UITableViewRowAnimationNone];
}

//this function has two purposes:
//purpose number 1: real names are not on the "owner" part of the results, and must be fetched elsewhere anyway.
//purpose number 2: make a cache of fetched names, the realnames API is rate-limited to 60 requests per hour, thus caching we can at least display names that repeat often, like "Facebook" and "Google"
- (void)fetchRealNameForLogin:(NSString*)login usingURL:(NSString*)stringUrl
{
    if([login isEqualToString:@"NULL USERNAME"] || [stringUrl isEqualToString:@"NULL PROFILE URL"]) return; //something is null, can't work with this
    
    if([self.namesCache valueForKey:login] != nil) return; //already has this name, ignore.
    
    [self.namesCache setValue:@"fetching" forKey:login];
    
    NSURL *url = [NSURL URLWithString:stringUrl];
    NSURLRequest *request = [NSURLRequest requestWithURL:url];
    [NSURLConnection sendAsynchronousRequest:request
                                       queue:[NSOperationQueue mainQueue]
                           completionHandler:^(NSURLResponse *response,
                                               NSData *data, NSError *connectionError)
     {
         if (data.length > 0 && connectionError == nil)
         {
             NSDictionary *resultsDictionary = [NSJSONSerialization JSONObjectWithData:data
                                                                               options:0
                                                                                 error:NULL];
             id realName = [resultsDictionary objectForKey:@"name"];
             if(realName == [NSNull null] || realName == nil)
             {
                 [self.namesCache removeObjectForKey:login];
             }
             else
             {
                 [self.namesCache setObject:realName forKey:login];
             }
         }
         else
         {
             [self.namesCache removeObjectForKey:login];
         }
     }];
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    // Do any additional setup after loading the view, typically from a nib.
    self.detailViewController = (DetailViewController *)[[self.splitViewController.viewControllers lastObject] topViewController];
    
    self.namesCache = [[NSMutableDictionary alloc] init];
    self.projectSearchResults = [[NSMutableArray alloc] init];
    self.rows = 0;
    self.pagesFetched = 0;
    self.APIbusy = NO;
    self.endedFetching = NO;
    self.highestRowRequested = 0;
    [self fetchGHSearchPage:self.pagesFetched+1];
}

- (void)viewWillAppear:(BOOL)animated {
    self.clearsSelectionOnViewWillAppear = self.splitViewController.isCollapsed;
    [super viewWillAppear:animated];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
}

#pragma mark - Segues

- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    if ([[segue identifier] isEqualToString:@"showDetail"]) {
        NSIndexPath *indexPath = [self.tableView indexPathForSelectedRow];
        NSInteger currentPage = indexPath.row/self.rowsPerPage;
        NSInteger currentRowInPage = indexPath.row%self.rowsPerPage;
        GHSearchResults* searchResultsPage = self.projectSearchResults[currentPage];
        GHProjectResult* project = searchResultsPage.projects[currentRowInPage];
        DetailViewController *controller = (DetailViewController *)[[segue destinationViewController] topViewController];
        [controller setPullRequestsURL:[NSString stringWithFormat:@"https://api.github.com/repos/%@/%@/pulls?state=all", project.owner.username, project.name]];
        controller.navigationItem.leftBarButtonItem = self.splitViewController.displayModeButtonItem;
        controller.navigationItem.leftItemsSupplementBackButton = YES;
    }
}

#pragma mark - Table View

- (void)scrollViewDidScroll:(UIScrollView *)tableView
{
    if(_APIbusy == YES || _endedFetching == YES || self.rows == 0) return;
    
    if(tableView.contentOffset.y > tableView.contentSize.height - tableView.frame.size.height*2)
    {
        //during tests on the Simulator at least, sometimes the condition above triggered erroneously, thus the check below was added to avoid wasting API calls when the user didn't actually scrolled there.
        CGFloat cellsPer2Screens = (tableView.frame.size.height*2)/self.cellHeight;
        if(self.rows - self.highestRowRequested < cellsPer2Screens)
        {
            [self fetchGHSearchPage:self.pagesFetched+1];
        }
    }
}

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
    return 1;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    return self.rows;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:@"Cell" forIndexPath:indexPath];

    NSInteger currentPage = indexPath.row/self.rowsPerPage;
    NSInteger currentRowInPage = indexPath.row%self.rowsPerPage;
    GHSearchResults* searchResultsPage = self.projectSearchResults[currentPage];
    GHProjectResult* project = searchResultsPage.projects[currentRowInPage];
    ((UILabel *)[cell viewWithTag:1]).text = project.name;
    ((UILabel *)[cell viewWithTag:2]).text = project.projectDescription;
    ((UILabel *)[cell viewWithTag:3]).text = [NSString stringWithFormat:@"%d", project.forks];
    ((UILabel *)[cell viewWithTag:4]).text = [NSString stringWithFormat:@"%d", project.stars];
    ((UILabel *)[cell viewWithTag:5]).text = project.owner.username;
    NSString* realName = [self.namesCache objectForKey:project.owner.username];
    if(realName == nil) ((UILabel *)[cell viewWithTag:6]).text = @"";
    else ((UILabel *)[cell viewWithTag:6]).text = realName;
    [[cell viewWithTag:7] sd_setImageWithURL:[NSURL URLWithString:project.owner.avatarURL]
                      placeholderImage:[UIImage imageNamed:@"placeholder.png"]];
    
    self.cellHeight = cell.frame.size.height;
    if(indexPath.row > self.highestRowRequested) self.highestRowRequested = indexPath.row;
    
    return cell;
}

@end
