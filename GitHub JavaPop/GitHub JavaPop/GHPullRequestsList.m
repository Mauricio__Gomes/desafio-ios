//
//  GHPullRequestsList.m
//  GitHub JavaPop
//
//  Created by Maurício Gomes on 9/11/16.
//  Copyright © 2016 Mauricio Gomes. All rights reserved.
//

#import "GHPullRequestsList.h"
#import "GHPullRequest.h"

@implementation GHPullRequestsList

- (id)initWithArray:(NSArray*)resultsArray
{
    self = [super init];
    
    _requestsArray = [[NSMutableArray alloc] init];
    int counter = 0;
    while(counter < resultsArray.count)
    {
        [_requestsArray addObject:[[GHPullRequest alloc] initWithDictionary:resultsArray[counter]]];
        ++counter;
    }
    
    return self;
}

@end
