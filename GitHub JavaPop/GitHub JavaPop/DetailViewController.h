//
//  DetailViewController.h
//  GitHub JavaPop
//
//  Created by Maurício Gomes on 9/9/16.
//  Copyright © 2016 Mauricio Gomes. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "GHPullRequestsList.h"

@interface DetailViewController : UIViewController

@property (nonatomic) NSInteger rows;
@property (nonatomic) NSInteger rowsPerPage;
@property (nonatomic) NSInteger pagesFetched;
@property (nonatomic) NSInteger closedPulls;
@property (nonatomic) NSInteger openPulls;
@property (nonatomic) NSInteger highestRowRequested;
@property (nonatomic) CGFloat cellHeight;
@property (nonatomic) BOOL APIbusy;
@property (nonatomic) BOOL loaded;
@property (nonatomic) BOOL endedFetching;
@property (strong, nonatomic) NSString* queueSetPullRequestURL;
@property (strong, nonatomic) NSString* pullURL;
@property (strong, nonatomic) NSMutableArray* pullRequestsPages;
@property (weak, nonatomic) IBOutlet UITableView *tableView;
@property (weak, nonatomic) IBOutlet UILabel *headerMessage;

- (void)setPullRequestsURL:(id)pullURL;

@end

