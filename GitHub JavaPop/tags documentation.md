We will populate the cells using the "tag" method.

This is the documentation of the tags:

Main Cells (Repository List)

Title = 1
Description = 2
Forks = 3
Stars = 4
Author Nickname = 5
Author Name = 6
Author Avatar = 7

Pull Request Cells (Pull requests List)

Title = 1
Description = 2
Author Nickname = 3
Author Avatar = 4